import Search from './models/Search.js';
import Recipe from './models/Recipe.js';
import List from './models/List.js';
import Likes from './models/Likes.js';
import {elements, renderLoader, clearLoader} from './views/base.js';
import * as searchView from './views/searchView.js';
import * as recipeView from './views/recipeView.js';
import * as listView from './views/listView.js';
import * as likesView from './views/likesView.js';

/** Global state of app
 *  - search object
 *  - recipe object
 *  - shopping list object
 *  - liked recipes
*/


const state = {};


//SEARCH CONTROLLER
const controlSearch = async () =>{
    //1. get query from view 
    const query = searchView.getInput();
    
    if(query){
        //2. create new search object and add to state
        state.search = new Search(query); 

        //3. prepare UI for results
        searchView.clearInput();
        searchView.clearResults();
        renderLoader(elements.searchRes);

        try{
            //4. search for recipes
            await state.search.getData();
            //5. render results on UI
            clearLoader();
            searchView.renderResults(state.search.results);
        }catch(error){
            clearLoader();
            console.log(error);
        }
    }
};

elements.searchForm.addEventListener('submit', event => {
    event.preventDefault();
    controlSearch();
});

elements.searchResPages.addEventListener('click', event =>{
    const btn = event.target.closest('.btn-inline');
    if(btn){
        const goToPage = parseInt(btn.dataset.goto, 10);
        searchView.clearResults();
        searchView.renderResults(state.search.results, goToPage);
    }
});

//RECIPE CONTROLLER
const controlRecipe = async () =>{
    //get id from url
    const id = window.location.hash.replace('#', '');

    if(id){
        //prepare UI for changes
        recipeView.clearRecipe();
        renderLoader(elements.recipe);

        //highlight selected search item
        if(state.search) searchView.highlightSelected(id);
        //create new recipe object
        state.recipe = new Recipe(id);
        try{
            //get recipe data
            await state.recipe.getRecipe();
            state.recipe.parseIngredients();
            //calc servigns and time
            state.recipe.calcTime();
            state.recipe.calcServing();
            //render recipe on UI
            clearLoader();
            recipeView.renderRecipe(
                state.recipe, 
                state.likes.isLiked(id)
                );
        }catch (error){
            console.log(error);
        }
    }
};

//SEARCH CONTROLLER
const controlList = () =>{
    
    //create new list if there is none yet
    if(!state.list) state.list = new List();

    //add each ingredient to list
    state.recipe.ingredients.forEach(current =>{
        const item = state.list.addItem(current.count, current.unit, current.ingredient);
        listView.renderItem(item);
    });
}

elements.shopping.addEventListener('click', event =>{

    const id = event.target.closest('.shopping__item').dataset.itemid;

    if(event.target.matches('.shopping__delete, .shopping__delete *')){
       
        //delete item from state
        state.list.deleteItem(id);

        //delete item from ui
        listView.deleteItem(id);

    }else if(event.target.matches('.shopping__count-value')){
        const val = parseFloat(event.target.value, 10);
        state.list.updateCount(id, val);
    }
});


// state.likes = new Likes();
// likesView.toggleLikeMenu(state.likes.getNumLikes());


/** 
 * LIKE CONTROLLER
 */
const controlLike = () => {
    if (!state.likes) state.likes = new Likes();
    const currentID = state.recipe.id;

    // User has NOT yet liked current recipe
    if (!state.likes.isLiked(currentID)) {
        // Add like to the state
        const newLike = state.likes.addLikes(
            currentID,
            state.recipe.title,
            state.recipe.author,
            state.recipe.img
        );
        // Toggle the like button
        likesView.toggleLikeBtn(true);

        // Add like to UI list
        likesView.renderLike(newLike);

    // User HAS liked current recipe
    } else {
        // Remove like from the state
        state.likes.deleteLike(currentID);

        // Toggle the like button
        likesView.toggleLikeBtn(false);

        // Remove like from UI list
        likesView.deleteLike(currentID);
    }
    likesView.toggleLikeMenu(state.likes.getNumLikes());
};

// Restore liked recipes on page load
window.addEventListener('load', () => {
    state.likes = new Likes();
    
    // Restore likes
    state.likes.readStorage();

    // Toggle like menu button
    likesView.toggleLikeMenu(state.likes.getNumLikes());

    // Render the existing likes
    state.likes.likes.forEach(like => likesView.renderLike(like));
});



['hashchange', 'load'].forEach(event => window.addEventListener(event, controlRecipe));

//handling recipe button clicks
elements.recipe.addEventListener('click', event =>{
    if(event.target.matches('.btn-decrease, .btn-decrease *')){
        if(state.recipe.servings > 1){
        state.recipe.updateServings('dec');
        recipeView.updateServingsIngredients(state.recipe);
    }
    }else if (event.target.matches('.btn-increase, .btn-increase *')){
        state.recipe.updateServings('inc');
        recipeView.updateServingsIngredients(state.recipe);
    }else if(event.target.matches('.recipe__btn--add, .recipe__btn--add *')){
        controlList();
    }else if(event.target.matches('.recipe__love, .recipe__love *')){
        controlLike();
    }
});
