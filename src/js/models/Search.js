import axios from 'axios'; //importing package

class Search {
    constructor(query){
        this.query = query; 
    }

    async getData (){
        try{
            const response = await axios.get(`https://forkify-api.herokuapp.com/api/search?q=${this.query}`);
            this.results = response.data.recipes;
        } catch (err){
            console.log(err);
        }
    }
};









export default Search; 







