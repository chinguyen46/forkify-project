
import axios from 'axios'; //importing package

class Recipe{
    constructor(id){ 
        this.id = id; 
    }

    async getRecipe (){
        try{
            const response = await axios.get(`https://forkify-api.herokuapp.com/api/get?rId=${this.id}`);
            this.title = response.data.recipe.title;
            this.author = response.data.recipe.publisher;
            this.img = response.data.recipe.image_url;
            this.url = response.data.recipe.source_url;
            this.ingredients = response.data.recipe.ingredients;
        } catch (err){
            console.log(err);
        }
    }

    calcTime(){
        //assuming we need 15 mins per ingredients
        const numIng = this.ingredients.length;
        const periods = Math.ceil(numIng/3);
        this.time = periods * 15;
    }

    calcServing(){
        this.servings = 4;
    }

    parseIngredients(){
        const unitsLong = ['tablespoons', 'tablespoon', 'ounces', 'ounce', 'teaspoons', 'teaspoon', 'cups', 'pounds'];
        const unitsShort = ['tbsp', 'tsbp', 'oz', 'oz', 'tsp', 'tsp', 'cup', 'pound'];
        const units = [...unitsShort, 'kg', 'g'];

        const newIngredients = this.ingredients.map( current =>{
            //uniform units
            let ingredient = current.toLowerCase();
            unitsLong.forEach((current, index) =>{
                ingredient = ingredient.replace(current, unitsShort[index]);
            });
            //remove parentheses 
            ingredient = ingredient.replace(/ *\([^)]*\) */g, ' ');

            //parse ingredients into count unit and ingredients
            const arrIng = ingredient.split(' ');
            const unitIndex = arrIng.findIndex(current2 => units.includes(current2));

            let objIng;
            if(unitIndex > -1){

                const arrCount = arrIng.slice(0, unitIndex);

                let count; 
                if(arrCount.length === 1){
                    count = eval(arrIng[0].replace('-', '+'));
                }else{
                    count = eval(arrIng.slice(0, unitIndex).join('+'));
                }
                objIng = {
                    count,
                    unit: arrIng[unitIndex],
                    ingredient: arrIng.slice(unitIndex + 1).join(' ')
                }
            }else if(parseInt(arrIng[0], 10)){
                objIng = {
                    count: parseInt(arrIng[0], 10),
                    unit: '',
                    ingredient: arrIng.slice(1).join(' ')
                }
            }else if (unitIndex === -1){
                objIng = {
                    count: 1,
                    unit: '',
                    ingredient
                }
            }
            return objIng;
        });

        this.ingredients = newIngredients;
    }


    updateServings(type){
        const newServings = type === 'dec' ? this.servings - 1 : this.servings + 1;

        this.ingredients.forEach(ing => {
            ing.count *= (newServings/ this.servings);
        });

        this.servings = newServings;
    }

};





export default Recipe; 
