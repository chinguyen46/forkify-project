import uniqid from 'uniqid';

class List{
    constructor(){
        this.items = [];
    }

    addItem(count, unit, ingredient){
        const item = {
            count,
            unit,
            ingredient,
            id: uniqid()
        }

        this.items.push(item);
        return item;
    }

    deleteItem(id){
        const index = this.items.findIndex(current => current.id === id);
        this.items.splice(index, 1);
    }

    updateCount(id, newCount){
        this.items.find(current => current.id === id).count = newCount;
    }


};


export default List; 