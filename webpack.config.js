const path = require('path');
const HtmlWebPackPlugin = require ('html-webpack-plugin');


module.exports = {
	entry: ['./src/js/index.js'], //entry point for creating the bundle
	output: {
		path: path.resolve(__dirname, './dist'), //webpack will ouput file into this directory with bundle.js file
		filename: './js/bundle.js'
	},
	mode: 'development',
	 module: {
	   rules:
	    [{
	       test: /\.js$/,
	       exclude: /node_modules/,
	       use: ['babel-loader'] 
	   	}]
	 },
	 devServer:{
		 contentBase: './dist'
	 },
	 plugins: [
		 new HtmlWebPackPlugin ({ filename: 'index.html', template: './src/index.html' })
	 ]
};
